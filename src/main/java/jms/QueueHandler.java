package jms;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by rmanganye on 2017/02/28.
 */
public class QueueHandler {

    private Connection connection;
    private Channel channel;

    public QueueHandler() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        connection = factory.newConnection();
        channel = connection.createChannel();
    }

    public void publishRegisterProfile(String profileModel,String queue) throws IOException, TimeoutException {
        channel.queueDeclare(queue, false, false, false, null);
        channel.basicPublish("", queue, null, profileModel.getBytes());
    }

 public void publishRegisterAddress(String addressModel,String queue) throws IOException, TimeoutException {
        channel.queueDeclare(queue, false, false, false, null);
        channel.basicPublish("", queue, null, addressModel.getBytes());
    }

    public void CloseConnection() throws IOException {
        connection.close();
    }
}
