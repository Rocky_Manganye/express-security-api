package services;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import model.LoginRequest;
import model.RegisterRequest;
import model.domain.Profile;
import model.domain.Province;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Created by rmanganye on 2017/02/24.
 */
@Path("/security")
@Api( value = "/security", description = "Manage Security" )
public interface IAuthenticateService {

    @POST
    @Path("/register")
    @Produces("application/json")
    @Consumes("application/json")
    @ApiOperation(
            value = "Register a user",
            notes = "register a single user",
            response = Response.class,
            responseContainer = "IAuthenticateService"
    )
    Response register(LoginRequest request) throws IOException, TimeoutException, SQLException, ClassNotFoundException;

    @POST
    @Path("/login")
    @Produces("application/json")
    @Consumes("application/json")
    Response login(LoginRequest request) throws IOException, TimeoutException;

    @POST
    @Path("/profile")
    @Produces("application/json")
    @Consumes("application/json")
    Response profile(Profile profile) throws IOException , TimeoutException, ClassNotFoundException;

    @POST
    @Path("/add/provinces")
    @Produces("application/json")
    @Consumes("application/json")
    Response province(List<Province> provinces) throws IOException, SQLException, ClassNotFoundException;



}
