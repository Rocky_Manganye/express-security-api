package serviceImpl;

import dao.AccountDaoHandler;
import model.AuthHeader;
import model.LoginRequest;
import model.RegisterRequest;
import model.domain.Login;
import model.domain.Profile;
import model.domain.Province;
import model.response.RegisterResponse;
import model.response.ResponseType;
import services.IAuthenticateService;
import utils.ResponseUtil;
import utils.TokenUtil;
import validator.RegisterValidator;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.core.Response;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeoutException;

/**
 * Created by rmanganye on 2017/02/24.
 */

public class AuthenticateService implements IAuthenticateService {

    public Response register(LoginRequest registerRequest) throws IOException, TimeoutException, SQLException, ClassNotFoundException {

        System.out.println("Enter");
        AccountDaoHandler accountDaoHandler = new AccountDaoHandler();
        Response response;

        String status = accountDaoHandler.register(registerRequest);
        System.out.println("status : " + status);
        String token = TokenUtil.GenerateToke(registerRequest.getPassword(), registerRequest.getEmail());
        System.out.println("token : " + token);
      if(status.toLowerCase().equalsIgnoreCase("pass")){
          int login = accountDaoHandler.login(registerRequest);
          System.out.println("token : " + token);
          List<AuthHeader> headers = new ArrayList<>();
          headers.add(new AuthHeader("express-token", token));
          response = ResponseUtil.MappResponse(
                  Response.Status.OK,
                  headers, String.valueOf(login),
                  ResponseType.Success);
      }else{
          response = ResponseUtil.MappResponse(Response.Status.BAD_REQUEST,
                  new ArrayList<AuthHeader>(),
                  "invalid request parameters", ResponseType.Error);
      }

        return response;
    }

    @Override
    public Response login(LoginRequest request) throws IOException, TimeoutException {
        try {
            AccountDaoHandler accountDaoHandler = new AccountDaoHandler();
            int login = accountDaoHandler.login(request);

            String token = TokenUtil.GenerateToke(request.getPassword(), request.getEmail());
            List<AuthHeader> headers = new ArrayList<>();
            headers.add(new AuthHeader("express-token", token));
            return ResponseUtil.MappResponse(
                    Response.Status.OK,
                    headers, String.valueOf(login),
                    ResponseType.Success);
        } catch (Exception e) {
            return Response.status(Response.Status.EXPECTATION_FAILED).build();
        }
    }

    @Override
    public Response profile(Profile profile) throws IOException, TimeoutException, ClassNotFoundException {
        try{
            AccountDaoHandler accountDaoHandler = new AccountDaoHandler();
            String profileSaveStatus = accountDaoHandler.createProfile(profile);

            return ResponseUtil.MappResponse(
                    Response.Status.OK,
                    null, profile.getLoginId(),
                    ResponseType.Success);
        }catch (Exception e){
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @Override
    public Response province(List<Province> provinces) throws IOException, SQLException, ClassNotFoundException {
        AccountDaoHandler accountDaoHandler = new AccountDaoHandler();
        for (Province province: provinces ) {
            accountDaoHandler.addProvince(province);
        }

        return Response.status(Response.Status.OK).build();
    }
}
