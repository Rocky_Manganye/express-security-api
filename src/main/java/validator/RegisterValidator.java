package validator;

import model.RegisterRequest;
import org.apache.commons.lang.StringUtils;

/**
 * Created by rmanganye on 2017/02/24.
 */
public class RegisterValidator {
    public static boolean Validate(RegisterRequest registerRequest) {
        return !(StringUtils.isEmpty(registerRequest.getUsername())
                || StringUtils.isEmpty(registerRequest.getPassword()));
    }
}
