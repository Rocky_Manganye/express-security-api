import io.swagger.jaxrs.config.BeanConfig;
import serviceImpl.AuthenticateService;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by rmanganye on 2017/02/24.
 */
@ApplicationPath("/authenticate")
public class ExpressPickApplication extends Application {

    public ExpressPickApplication() {
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setVersion("1.0.2");
        beanConfig.setSchemes(new String[]{"http"});
        beanConfig.setHost("localhost:1994");
        beanConfig.setBasePath("/authenticate");
        beanConfig.setResourcePackage("io.swagger.resources");
        beanConfig.setScan(true);
    }

    @Override
    public Set<Class<?>> getClasses() {
        HashSet resources = new HashSet();
        resources.add(AuthenticateService.class);
        resources.add(io.swagger.jaxrs.listing.ApiListingResource.class);
        resources.add(io.swagger.jaxrs.listing.SwaggerSerializers.class);
        return resources;
    }
}
