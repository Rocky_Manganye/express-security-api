package utils;

import static utils.Md5Encryption.DecryptText;
import static utils.Md5Encryption.EncryptText;

/**
 * Created by rmanganye on 2017/02/28.
 */
public class TokenUtil {
    public static String GenerateToke(String password, String username) {
        System.out.println("password " + password.toString());
        System.out.println("username " + username.toString());
        String token =  EncryptText("username:" + username
                + ",password:"+ Encoder.encode(password));
        System.out.println("token " + token.toString());
        return token;
    }

    public static String[] BreakDownToken(String token) {

        return new String[]{
                token.split(",")[0].split(":")[1],
                Encoder.decode(token.split(",")[1].split(":")[1])
        };
    }
}
