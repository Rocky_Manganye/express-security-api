package utils;

import org.apache.commons.codec.binary.Base64;

/**
 * Created by rmanganye on 2017/02/24.
 */
public class Encoder {
    public static String encode(String plaintext){
        byte[] encodedBytes = Base64.encodeBase64(plaintext.getBytes());
       return new String(encodedBytes);
    }

    public static String decode(String encodedTex){
        byte[] decodeBase = Base64.decodeBase64(encodedTex.getBytes());
       return new String(decodeBase);
    }
}
