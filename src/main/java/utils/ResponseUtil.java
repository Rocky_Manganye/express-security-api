package utils;

import model.AuthHeader;
import model.response.RegisterResponse;
import model.response.ResponseType;

import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by rmanganye on 2017/02/28.
 */
public class ResponseUtil {
    public static Response MappResponse(Response.Status status, List<AuthHeader> headers, String message, ResponseType success){
        Response.ResponseBuilder response  = Response.accepted();
        if(status == Response.Status.OK){

            for (AuthHeader header : headers) {
                response.header(header.getKey(), header.getValue());
                System.out.println("==============================");
                System.out.println("Key : " + header.getKey());
                System.out.println("value : " + header.getValue());
                System.out.println("==============================");
            }
        }

        System.out.println("status " + status.toString());
        Response.status(status);
        System.out.println("==============================");

        RegisterResponse body = new RegisterResponse(message);

        response.entity(body);
        return response.build();
    }
}
