package utils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by rmanganye on 2017/02/28.
 */
public class PropertiesUtil {
    private static InputStream in;
    public static String getPropertiyByKey(String key) throws Exception{
        in = new FileInputStream("authentication.properties");
        Properties prop = new Properties();
        prop.loadFromXML(in);
        return prop.getProperty(key);
    }
}
