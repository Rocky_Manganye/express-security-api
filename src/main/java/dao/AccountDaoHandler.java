package dao;

import jms.QueueHandler;
import model.LoginRequest;
import model.RegisterRequest;
import model.domain.Login;
import model.domain.Profile;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.sql.*;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.domain.Province;

/**
 * Created by rmanganye on 2017/02/28.
 */

public class AccountDaoHandler {

   /* private static EntityManagerFactory emf = Persistence
            .createEntityManagerFactory("authenticate_unit");
    private static EntityManager em = emf.createEntityManager();
*/

    private Connection connection;
    private Statement statement;

    public AccountDaoHandler() throws ClassNotFoundException, SQLException {
       // Class.forName("com.mysql.jdbc.Driver");
        try
        {   // Load driver class
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch (java.lang.ClassNotFoundException e) {
            System.err.println("ClassNotFoundException : " +e);
        }
        connection = DriverManager.getConnection("jdbc:mysql://expressdrop.cyaiggjokqxz.us-east-1.rds.amazonaws.com:3306/ExpressDrop","Rocksta","Ntshuxeko");
        statement = connection.createStatement();
    }

    public  String register(LoginRequest request) throws IOException, TimeoutException, SQLException {
        String insertTableSQL = "INSERT INTO login"
                + "(id, username, password) VALUES"
                + "(?,?,?)";


        System.out.println("SQL : " + insertTableSQL);
       try {
          PreparedStatement preparedStatement = connection.prepareStatement(insertTableSQL);
           preparedStatement.setInt(1, new Random().nextInt(580000));
           preparedStatement.setString(2, request.getEmail());
           preparedStatement.setString(3, request.getPassword());

           preparedStatement.executeUpdate();

           System.out.println("Record is inserted into login table!");

           return "Pass";
       }catch (Exception e){
           System.out.println("Record is not inserted into login table! " + e.fillInStackTrace());
            return "fail";
       }
    }

    public int login(LoginRequest request) throws SQLException {
        String selectSQL = "SELECT * FROM login WHERE username ='" + request.getEmail() + "' and password ='" + request.getPassword() + "'";
        System.out.println("SQL : " + selectSQL);
        ResultSet rs = statement.executeQuery(selectSQL );
        while (rs.next()) {
            int userid = rs.getInt("id");
            return userid;
        }
        return -1;
    }

    public void addProvince(Province province) {
        String insertTableSQL = "INSERT INTO province"
                + "(id,name) VALUES"
                + "(?,?)";
        System.out.println("SQL : " + insertTableSQL);
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1, UUID.randomUUID().toString());
            preparedStatement.setString(2, province.getName());
            preparedStatement.executeUpdate();

            System.out.println("Record is inserted into province table! ");
        }catch (Exception e){
            System.out.println("Record is not inserted into login table! " + e.fillInStackTrace());
        }
    }

    public String createProfile(Profile profile)
    {

        String insertTableSQL = "INSERT INTO profile"
                + "(id,firstName,lastName,cellNumber,idNumber,gender,loginId) VALUES"
                + "(?,?,?,?,?,?,?)";

        System.out.println("SQL : " + profile.toString());
        try
        {
            PreparedStatement preparedStatement = connection.prepareStatement(insertTableSQL);

            preparedStatement.setString(1, profile.getId());
            preparedStatement.setString(2, profile.getFirstName());
            preparedStatement.setString(3, profile.getLastName());
            preparedStatement.setString(4, profile.getCellNumber());
            preparedStatement.setString(5, profile.getIdNumber());
            preparedStatement.setString(6, profile.getGender());
            preparedStatement.setString(7, profile.getLoginId());
            preparedStatement.executeUpdate();

            System.out.println("Record is inserted into province table! ");
            return "pass";
        } catch (Exception e) {
            System.out.println("Record is not inserted into login table! " + e.fillInStackTrace());
        }
        return "fail";
    }
}
