package model.domain;

/**
 * Created by rmanganye on 2017/04/06.
 */
public class Province {
    private String id;
    private String name;

    public Province() {
    }

    public Province(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
