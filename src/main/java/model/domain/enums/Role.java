package model.domain.enums;

/**
 * Created by rmanganye on 2017/02/24.
 */
public enum Role {
    Admin,
    User
}
