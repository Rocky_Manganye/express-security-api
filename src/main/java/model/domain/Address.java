package model.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Created by rmanganye on 2017/02/28.
 */
@Entity
public class Address {

    @Id
    private int id;
    private String streetName;
    private int houseNumber;
    private String suburb;
    private String city;
    private String country;
    private int zipCode;

    public Address() {}

    public Address(String streetName, int houseNumber, String suburb, String city,
                   String country, int zipCode) {
        this.streetName = streetName;
        this.houseNumber = houseNumber;
        this.suburb = suburb;
        this.city = city;
        this.country = country;
        this.zipCode = zipCode;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

}
