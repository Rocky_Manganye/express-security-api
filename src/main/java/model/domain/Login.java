package model.domain;

import model.domain.enums.Role;

import javax.persistence.*;

/**
 * Created by rmanganye on 2017/02/24.
 */
@Entity
@NamedQuery(name = "login",query = "select e from Login e where e.username = :username and e.password = :password")
public class Login {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int id;
    private String username;
    private String password;

    public Login() {
    }

    public Login(String username, String password) {
        this.username = username;
        this.password = password;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
