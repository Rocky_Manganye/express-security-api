package model.domain;

import java.util.UUID;

/**
 * Created by rmanganye on 2017/02/28.
 */

public class Profile {

    private String id;
    private String loginId;
    private String firstName;
    private String lastName;
    private String cellNumber;
    private String IdNumber;
    private String gender;

    public Profile() {
    }

        public Profile( String firstName, String lastName,String cellNumber,
                       String idNumber, String gender, String loginId) {
        this.firstName = firstName;
        this.loginId = loginId;
        this.lastName = lastName;
        this.cellNumber = cellNumber;
        this.IdNumber = idNumber;
        this.gender = gender;
        this.id = UUID.randomUUID().toString();
    }

    public String getIdNumber() {
        return IdNumber;
    }

    public void setIdNumber(String idNumber) {
        IdNumber = idNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCellNumber() {
        return cellNumber;
    }

    public void setCellNumber(String cellNumber) {
        this.cellNumber = cellNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLoginId() {
        return loginId;
    }

    public void setLoginId(String loginId) {
        this.loginId = loginId;
    }

    @Override
    public String toString() {
        return "Profile{" +
                "id='" + id + '\'' +
                ", loginId='" + loginId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", cellNumber='" + cellNumber + '\'' +
                ", IdNumber='" + IdNumber + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }
}
