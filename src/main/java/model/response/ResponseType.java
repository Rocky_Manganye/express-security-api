package model.response;

/**
 * Created by rmanganye on 2017/02/28.
 */
public enum ResponseType {
    Error,Exception,Success
}
