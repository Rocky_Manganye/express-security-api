package model.response;

/**
 * Created by rmanganye on 2017/02/28.
 */
public class BaseResponse {
    private String message;
    private ResponseType responseType;
    public Object ResponseObject;

    public BaseResponse() {
    }

    public BaseResponse(String message, ResponseType responseType, Object responseObject) {
        this.message = message;
        this.responseType = responseType;
        ResponseObject = responseObject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseType getResponseType() {
        return responseType;
    }

    public void setResponseType(ResponseType responseType) {
        this.responseType = responseType;
    }

    public Object getResponseObject() {
        return ResponseObject;
    }

    public void setResponseObject(Object responseObject) {
        ResponseObject = responseObject;
    }
}
