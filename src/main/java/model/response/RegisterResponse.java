package model.response;

/**
 * Created by rmanganye on 2017/02/28.
 */
public class RegisterResponse  {
    private String loginKey;

    public RegisterResponse(String loginKey) {
        this.loginKey = loginKey;
    }

    public RegisterResponse() {
    }

    public String getLoginKey() {
        return loginKey;
    }

    public void setLoginKey(String loginKey) {
        this.loginKey = loginKey;
    }
}
