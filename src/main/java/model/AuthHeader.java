package model;

/**
 * Created by rmanganye on 2017/02/28.
 */
public class AuthHeader {
    private String key;
    private String value;

    public AuthHeader() {
    }

    public AuthHeader(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
