package model;


import model.domain.Address;

/**
 * Created by rmanganye on 2017/02/24.
 */
public class RegisterRequest {


    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String gender;
    private String email;
    private String cellNmber;
    private String IdNumber;
    private Address address;



    public RegisterRequest() {
    }


    public RegisterRequest(String firstName, String lastName, String username, String password, String gender, String email, String cellNmber, String idNumber, Address address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.gender = gender;
        this.email = email;
        this.cellNmber = cellNmber;
        IdNumber = idNumber;
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellNmber() {
        return cellNmber;
    }

    public void setCellNmber(String cellNmber) {
        this.cellNmber = cellNmber;
    }

    public String getIdNumber() {
        return IdNumber;
    }

    public void setIdNumber(String idNumber) {
        IdNumber = idNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
